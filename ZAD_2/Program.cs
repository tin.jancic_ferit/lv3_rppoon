﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD_2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator random_generator = MatrixGenerator.GetInstance();
            float[][] matrix = random_generator.CreateMatrix(2, 3);

            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j] + "\t");
                }
                Console.Write("\n");
            }
        }
    }
}
