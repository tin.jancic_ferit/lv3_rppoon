﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD_2
{
    class MatrixGenerator
    {
        public static MatrixGenerator instance;
        private Random random;

        private MatrixGenerator()
        {
            random = new Random();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        public float[][] CreateMatrix(int rows, int columns)
        {
            float[][] matrix = new float[columns][];
            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[i] = new float[rows];
            }

            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    matrix[i][j] = (float)random.NextDouble();
                }
            }
            return matrix;
        }
    }
}
